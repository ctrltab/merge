use crate::operation::Undoable;
use crate::tree_util::{dfs_explore, dfs_number, DFSCallback};
use im_rc::Vector;
use indextree::{Arena, NodeId};
use std::collections::{HashMap, VecDeque};

pub struct UndoHandler {
    arena: Arena<Box<dyn Undoable>>,
    current: NodeId,
    node_num: HashMap<usize, NodeId>,
}

impl UndoHandler {
    pub fn new() -> UndoHandler {
        let mut arena: Arena<Box<dyn Undoable>> = Arena::new();
        let root = arena.new_node(Box::new(()));

        let mut node_num = HashMap::new();
        node_num.insert(1, root);

        UndoHandler {
            arena,
            current: root,
            node_num,
        }
    }

    pub fn undo(&mut self, stack: &mut Vector<f32>) {
        if let Some(node) = self.arena.get_mut(self.current) {
            if let Some(parent) = node.parent() {
                self.current = parent;
                node.get().undo(stack);
            }
        }
    }

    pub fn redo(&mut self, stack: &mut Vector<f32>) {
        if let Some(node) = self.arena.get_mut(self.current) {
            if let Some(child) = node.first_child() {
                self.current = child;
                self.arena.get(child).unwrap().get().redo(stack);
            }
        }
    }

    /*pub fn left(&mut self, stack: &mut VecDeque<f32>) {
        if let Some(node) = self.arena.get_mut(self.current) {
            if let Some(sibling) = node.previous_sibling() {
                self.current = sibling;
                node.data.undo(stack);
                let sibling = self.arena.get_mut(sibling).unwrap();
                sibling.data.apply(stack);
            }
        }
    }

    pub fn right(&mut self, stack: &mut VecDeque<f32>) {
        if let Some(node) = self.arena.get_mut(self.current) {
            if let Some(sibling) = node.next_sibling() {
                self.current = sibling;
                node.data.undo(stack);
                let sibling = self.arena.get_mut(sibling).unwrap();
                sibling.data.apply(stack);
            }
        }
    }*/

    pub fn push(&mut self, undo: Box<dyn Undoable>) {
        let new = self.arena.new_node(undo);
        self.current.append(new, &mut self.arena);
        self.current = new;
        self.node_num.insert(self.node_num.len() + 1, new);
    }

    pub fn save(&self) {
        println!("{}", serde_yaml::to_string(&self.arena).unwrap())
    }

    pub fn goto(&mut self, target: usize, stack: &mut Vector<f32>) {
        let target = self.node_num.get(&target).unwrap();

        let numbered = dfs_number(*self.node_num.get(&1).unwrap(), &self.arena);

        let lca = numbered.find_lca(self.current, *target, &self.arena);

        for parent in self.current.ancestors(&self.arena) {
            self.arena.get(parent).unwrap().get().undo(stack);
            if parent == lca {
                break;
            }
        }

        let mut to_redo = VecDeque::new();

        for parent in target.ancestors(&self.arena) {
            if parent == lca {
                break;
            }
            to_redo.push_back(parent);
        }

        while !to_redo.is_empty() {
            let redo = to_redo.pop_back().unwrap();
            let redo = self.arena.get(redo).unwrap();
            let redo = redo.get();
            redo.redo(stack);
        }

        self.current = *target;
    }

    pub fn print(&mut self) {
        let mut depths = HashMap::new();
        depths.insert(*self.node_num.get(&1).unwrap(), 0 as usize);

        let k = if *self.node_num.get(&1).unwrap() == self.current {
            "<"
        } else {
            ""
        };

        println!("1{}", k);

        dfs_explore(
            *self.node_num.get(&1).unwrap(),
            &self.arena,
            &mut DFSCallback {
                traverse_tree_edge: |v, w| {
                    let depth = depths.get(&v).unwrap() + 1;
                    depths.insert(w, depth);

                    let mut s = String::new();

                    for _i in 0..depth {
                        s.push(' ');
                    }

                    let mut k = String::new();
                    if w == self.current {
                        k.push('<')
                    }

                    println!("{}{}{}", s, w, k);
                },
                traverse_non_tree_edge: |_, _| {},
                backtrack: |_, _| {},
            },
        );
    }
}
