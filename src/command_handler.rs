use crate::operation::Operation;
use std::borrow::{Borrow, BorrowMut};
use std::cell::{RefCell, RefMut};
use std::ops::{Deref, DerefMut};
use std::rc::Rc;

pub mod calculator_command_handler;
pub mod create_macro_command_handler;
pub mod root_command_handler;

pub trait CommandHandler {
    fn handle(&mut self, command: Rc<dyn Operation>);
    fn handle_command(&mut self, command: &str) {}
}

impl<T: CommandHandler> CommandHandler for Rc<RefCell<T>> {
    fn handle(&mut self, command: Rc<dyn Operation>) {
        let mut ref_cell_borrow: RefMut<T> = RefCell::borrow_mut(self);
        let client: &mut T = ref_cell_borrow.borrow_mut();
        client.handle(command)
    }

    fn handle_command(&mut self, command: &str) {
        let mut ref_cell_borrow: RefMut<T> = RefCell::borrow_mut(self);
        let client: &mut T = ref_cell_borrow.borrow_mut();
        client.handle_command(command)
    }
}
