#![feature(custom_attribute)]

use std::io;
use std::io::{stdin, Write};

mod command_handler;
mod operation;
mod tree_util;
mod undo;

use crate::command_handler::calculator_command_handler::CalculatorCommandHandler;
use crate::command_handler::create_macro_command_handler::CreateMacroCommandHandler;
use crate::command_handler::root_command_handler::RootCommandHandler;
use crate::command_handler::CommandHandler;
use crate::operation::operations::Operations;
use crate::operation::Operation;
use im_rc::HashMap;
use std::cell::RefCell;
use std::cmp::Ordering;
use std::collections::{BinaryHeap, VecDeque};
use std::rc::Rc;

extern crate serde;
extern crate serde_json;

fn main() {
    let operations = Rc::new(RefCell::new(Operations::new()));

    let mut handler = RootCommandHandler::new(Rc::clone(&operations));

    loop {
        let mut a = String::new();
        //  for _ in 0..handler_stack.len() {
        a.push('>');
        //  }
        print!("{} ", a);
        io::stdout().flush().unwrap();
        let buffer = &mut String::new();
        stdin().read_line(buffer);
        buffer.pop();

        let mut op_heap = BinaryHeap::new();

        for command in buffer.split_ascii_whitespace() {
            match command {
                _ => {
                    if let Some(operation) = operations.borrow().get(command) {
                        let num = operation.get_priority();
                        op_heap.push(OrderedOperation { operation, num });
                    }

                    handler.handle_command(command);
                }
            }
        }

        while !op_heap.is_empty() {
            let operation = op_heap.pop().unwrap();

            handler.handle(operation.operation);
        }
    }
}

struct OrderedOperation {
    operation: Rc<dyn Operation>,
    num: usize,
}

impl PartialOrd for OrderedOperation {
    fn partial_cmp(&self, other: &OrderedOperation) -> Option<Ordering> {
        self.num.partial_cmp(&other.num)
    }
}

impl PartialEq for OrderedOperation {
    fn eq(&self, other: &OrderedOperation) -> bool {
        self.num.eq(&other.num)
    }
}

impl Ord for OrderedOperation {
    fn cmp(&self, other: &Self) -> Ordering {
        self.num.cmp(&other.num)
    }
}

impl Eq for OrderedOperation {}
