use crate::command_handler::calculator_command_handler::CalculatorCommandHandler;
use crate::command_handler::create_macro_command_handler::CreateMacroCommandHandler;
use crate::command_handler::CommandHandler;
use crate::operation::operations::Operations;
use crate::operation::Operation;
use std::cell::RefCell;
use std::collections::HashMap;
use std::ops::Deref;
use std::rc::Rc;

pub struct RootCommandHandler {
    handlers: HashMap<String, Box<dyn CommandHandler>>,
    current_handler: String,
    operations: Rc<RefCell<Operations>>,
}

impl CommandHandler for RootCommandHandler {
    fn handle(&mut self, command: Rc<dyn Operation>) {
        self.handlers
            .get_mut(&self.current_handler)
            .unwrap()
            .handle(command);
    }

    fn handle_command(&mut self, command: &str) {
        let mut split = command.split(".");
        let mut split = split.peekable();

        if let Some((command, peek)) = { split.next().map(|s| (s, split.peek())) } {
            match command {
                "new" | "macro" if peek.is_some() => {
                    let name = split.next().unwrap();
                    let name = name.to_string();
                    self.current_handler = name.clone();

                    let handler: Box<dyn CommandHandler> = match command {
                        "new" => Box::new(CalculatorCommandHandler::new()),
                        "macro" => Box::new(CreateMacroCommandHandler::new(
                            name.clone(),
                            Rc::clone(&self.operations),
                        )),
                        _ => panic!(),
                    };
                    self.handlers.insert(name, handler);
                }

                "switch" if peek.is_some() => {
                    let name = split.next().unwrap();
                    let name = name.to_string();
                    self.current_handler = name.clone();
                }

                "stacks" => println!("{:?}", self.handlers.keys()),

                "exit" => {
                    panic!();
                }

                _ => {
                    self.handlers
                        .get_mut(&self.current_handler)
                        .unwrap()
                        .handle_command(command);
                }
            }
        }
    }
}

impl RootCommandHandler {
    pub fn new(operations: Rc<RefCell<Operations>>) -> RootCommandHandler {
        let mut handlers = HashMap::new();
        let default = CalculatorCommandHandler::new();

        handlers.insert(
            "default".to_string(),
            Box::new(default) as Box<dyn CommandHandler>,
        );

        RootCommandHandler {
            handlers,
            current_handler: "default".to_string(),
            operations,
        }
    }
}
