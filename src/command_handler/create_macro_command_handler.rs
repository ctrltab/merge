use crate::command_handler::CommandHandler;
use crate::operation::operations::Operations;
use crate::operation::{Operation, OperationError, Undoable};
use im_rc::Vector;
use serde::{Deserialize, Serialize};
use std::cell::RefCell;
use std::collections::VecDeque;
use std::io;
use std::io::{stdin, Write};
use std::ops::Deref;
use std::rc::Rc;

pub struct CreateMacroCommandHandler {
    to_exec: VecDeque<Rc<dyn Operation>>,
}

impl CommandHandler for CreateMacroCommandHandler {
    fn handle(&mut self, operation: Rc<dyn Operation>) {
        self.to_exec.push_back(operation);
    }
}

impl Operation for CreateMacroCommandHandler {
    fn apply(&self, stack: &Vector<f32>) -> Result<(f32, Box<Undoable>), OperationError> {
        let mut undo_vec: VecDeque<Box<dyn Undoable>> = VecDeque::new();
        let mut res = 0f32;

        let mut stack = stack.clone();

        for op in &self.to_exec {
            let ads = op.apply(&stack);
            let (ads, undo) = ads.ok().unwrap();
            undo.redo(&mut stack);

            res = ads;
            undo_vec.push_back(undo);
        }

        Ok((res, Box::new(MacroUndoable(undo_vec))))
    }
}

#[derive(Serialize, Deserialize)]
pub struct MacroUndoable(VecDeque<Box<dyn Undoable>>);

#[typetag::serde]
impl Undoable for MacroUndoable {
    fn undo(&self, stack: &mut Vector<f32>) {
        for undo in self.0.iter().rev() {
            undo.undo(stack);
        }
    }

    fn redo(&self, stack: &mut Vector<f32>) {
        for undo in self.0.iter() {
            undo.redo(stack);
        }
    }
}

impl CreateMacroCommandHandler {
    pub fn new(
        name: String,
        operations: Rc<RefCell<Operations>>,
    ) -> Rc<RefCell<CreateMacroCommandHandler>> {
        let res = CreateMacroCommandHandler {
            to_exec: VecDeque::new(),
        };

        let res = Rc::new(RefCell::new(res));

        operations
            .deref()
            .borrow_mut()
            .put(name, Rc::clone(&res) as Rc<Operation>);

        res
    }
}
