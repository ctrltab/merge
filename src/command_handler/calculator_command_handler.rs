use crate::command_handler::CommandHandler;
use crate::operation::Operation;
use crate::undo::UndoHandler;
use im_rc::Vector;
use std::rc::Rc;

pub struct CalculatorCommandHandler {
    undo: UndoHandler,
    stack: Vector<f32>,
}

impl CommandHandler for CalculatorCommandHandler {
    fn handle(&mut self, operation: Rc<dyn Operation>) {
        let result = operation.apply(&mut self.stack);

        if let Ok((val, a)) = result {
            println!("{:?}", val);
            a.redo(&mut self.stack);

            self.undo.push(a);
        }
    }

    fn handle_command(&mut self, command: &str) {
        match command {
            "stack" => {
                println!("{:?}", self.stack);
            }
            "undo" => {
                self.undo.undo(&mut self.stack);
            }
            "redo" => {
                self.undo.redo(&mut self.stack);
            }
            "print" => {
                self.undo.print();
            }
            "save" => {
                self.undo.save();
            }
            _ => {}
        }

        if command.starts_with("goto.") {
            let (_, arg) = command.split_at(5);
            self.undo.goto(arg.parse().unwrap(), &mut self.stack);
        }
    }
}

impl CalculatorCommandHandler {
    pub fn new() -> CalculatorCommandHandler {
        let undo = UndoHandler::new();
        let stack = Vector::new();

        CalculatorCommandHandler { undo, stack }
    }
}
