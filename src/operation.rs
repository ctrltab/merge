use im_rc::Vector;
use serde::{Deserialize, Serialize};
use std::cell::RefCell;
use std::ops::Deref;
use std::rc::Rc;

pub mod all_operator;
pub mod binary_operators;
pub mod duplicator_operator;
pub mod operations;

pub enum OperationError {
    NoError,
}

pub trait Operation {
    fn apply(&self, stack: &Vector<f32>) -> Result<(f32, Box<dyn Undoable>), OperationError>;

    fn get_priority(&self) -> usize {
        10
    }

    fn shortens_stack(&self) -> bool {
        false
    }
}

impl<T: Operation> Operation for Rc<T> {
    fn apply(&self, stack: &Vector<f32>) -> Result<(f32, Box<Undoable>), OperationError> {
        self.deref().apply(stack)
    }
}

impl<T: Operation> Operation for RefCell<T> {
    fn apply(&self, stack: &Vector<f32>) -> Result<(f32, Box<Undoable>), OperationError> {
        self.borrow().apply(stack)
    }
}

#[typetag::serde(tag = "type")]
pub trait Undoable {
    fn undo(&self, stack: &mut Vector<f32>);
    fn redo(&self, stack: &mut Vector<f32>);
}

#[typetag::serde(name = "none")]
impl Undoable for () {
    fn undo(&self, stack: &mut Vector<f32>) {}

    fn redo(&self, stack: &mut Vector<f32>) {}
}

impl Operation for () {
    fn apply(&self, stack: &Vector<f32>) -> Result<(f32, Box<dyn Undoable>), OperationError> {
        Ok((0.0, Box::new(())))
    }
}

#[derive(Copy, Clone, Deserialize, Serialize)]
pub struct PushNumber {
    pub i: f32,
}

impl Operation for PushNumber {
    fn apply(&self, stack: &Vector<f32>) -> Result<(f32, Box<dyn Undoable>), OperationError> {
        Ok((self.i, Box::new(*self)))
    }

    fn get_priority(&self) -> usize {
        20
    }
}

#[typetag::serde]
impl Undoable for PushNumber {
    fn undo(&self, stack: &mut Vector<f32>) {
        stack.pop_back();
    }

    fn redo(&self, stack: &mut Vector<f32>) {
        stack.push_back(self.i);
    }
}
