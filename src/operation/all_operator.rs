use crate::operation::OperationError::NoError;
use crate::operation::{Operation, OperationError, Undoable};
use im_rc::Vector;
use serde::{Deserialize, Serialize};
use std::rc::Rc;

pub struct AllOperator {
    operation: Rc<dyn Operation>,
}

impl AllOperator {
    pub fn new(operation: Rc<dyn Operation>) -> Option<AllOperator> {
        if operation.shortens_stack() {
            Some(AllOperator { operation })
        } else {
            None
        }
    }
}

#[derive(Serialize, Deserialize)]
pub struct AllOperatorUndoable {
    before: Vector<f32>,
    last_res: f32,
}

#[typetag::serde]
impl Undoable for AllOperatorUndoable {
    fn undo(&self, stack: &mut Vector<f32>) {
        stack.clear();
        stack.append(self.before.clone());
    }

    fn redo(&self, stack: &mut Vector<f32>) {
        stack.clear();
        stack.push_back(self.last_res);
    }
}

impl Operation for AllOperator {
    fn apply(&self, stack: &Vector<f32>) -> Result<(f32, Box<dyn Undoable>), OperationError> {
        let mut before = stack.clone();
        let mut stack_copy = stack.clone();

        let mut last_res = 0_f32;

        while stack_copy.len() > 1 {
            if let Ok((res, undo)) = self.operation.apply(&stack_copy) {
                last_res = res;
                undo.redo(&mut stack_copy);
            } else {
                return Err(NoError);
            }
        }

        Ok((last_res, Box::new(AllOperatorUndoable { before, last_res })))
    }

    fn shortens_stack(&self) -> bool {
        true
    }
}
