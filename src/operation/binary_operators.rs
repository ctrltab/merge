use crate::operation::{Operation, OperationError, Undoable};

use crate::operation::OperationError::NoError;

use im_rc::Vector;
use serde::{Deserialize, Serialize};

pub struct BinaryOperator<'a> {
    calc: &'a dyn Fn(f32, f32) -> f32,
    priority: usize,
}

#[derive(Copy, Clone, Deserialize, Serialize)]
struct BinaryOperatorUndoable {
    first: f32,
    second: f32,
    res: f32,
}

pub fn new<F>(calc: &F, priority: usize) -> BinaryOperator
where
    F: Fn(f32, f32) -> f32,
{
    BinaryOperator { calc, priority }
}

impl<'a> Operation for BinaryOperator<'a> {
    fn apply(&self, stack: &Vector<f32>) -> Result<(f32, Box<dyn Undoable>), OperationError> {
        if let Some(&first) = stack.back() {
            if let Some(&second) = stack.get(stack.len() - 2) {
                let res = (self.calc)(first, second);

                let undo = BinaryOperatorUndoable { first, second, res };

                return Ok((res, Box::new(undo)));
            }
        }

        Err(NoError)
    }

    fn get_priority(&self) -> usize {
        self.priority
    }

    fn shortens_stack(&self) -> bool {
        true
    }
}

#[typetag::serde]
impl Undoable for BinaryOperatorUndoable {
    fn undo(&self, stack: &mut Vector<f32>) {
        stack.pop_back();
        stack.push_back(self.second);
        stack.push_back(self.first);
    }

    fn redo(&self, stack: &mut Vector<f32>) {
        stack.pop_back();
        stack.pop_back();
        stack.push_back(self.res);
    }
}
