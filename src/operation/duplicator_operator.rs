use crate::operation::OperationError::NoError;
use crate::operation::{Operation, OperationError, Undoable};
use im_rc::Vector;
use serde::{Deserialize, Serialize};

pub struct DuplicatorOperator;

#[derive(Deserialize, Serialize)]
struct DuplicatorUndoable {
    i: f32,
}

impl Operation for DuplicatorOperator {
    fn apply(&self, stack: &Vector<f32>) -> Result<(f32, Box<dyn Undoable>), OperationError> {
        if let Some(&i) = stack.back() {
            return Ok((i, Box::new(DuplicatorUndoable { i })));
        }
        Err(NoError)
    }
}

#[typetag::serde]
impl Undoable for DuplicatorUndoable {
    fn undo(&self, stack: &mut Vector<f32>) {
        stack.pop_back();
    }

    fn redo(&self, stack: &mut Vector<f32>) {
        stack.push_back(self.i);
    }
}
