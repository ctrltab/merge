use crate::operation::all_operator::AllOperator;
use crate::operation::duplicator_operator::DuplicatorOperator;
use crate::operation::{binary_operators, Operation, OperationError, PushNumber, Undoable};
use im_rc::Vector;
use std::collections::HashMap;
use std::rc::Rc;

use serde::{Deserialize, Serialize};

pub struct Operations {
    map: HashMap<String, Rc<dyn Operation>>,
}

impl Operations {
    pub fn new() -> Operations {
        let mut map: HashMap<String, Rc<dyn Operation>> = HashMap::new();
        map.insert(
            "+".to_string(),
            Rc::new(binary_operators::new(&|f, s| f + s, 10)),
        );
        map.insert(
            "-".to_string(),
            Rc::new(binary_operators::new(&|f, s| f - s, 10)),
        );
        map.insert(
            "*".to_string(),
            Rc::new(binary_operators::new(&|f, s| f * s, 15)),
        );
        map.insert(
            "/".to_string(),
            Rc::new(binary_operators::new(&|f, s| f / s, 15)),
        );
        map.insert(
            "**".to_string(),
            Rc::new(binary_operators::new(&|f, s| f.powf(s), 20)),
        );
        map.insert(
            "mod".to_string(),
            Rc::new(binary_operators::new(&|f, s| f % s, 20)),
        );
        map.insert(
            "min".to_string(),
            Rc::new(binary_operators::new(&|f, s| f.min(s), 10)),
        );
        map.insert(
            "max".to_string(),
            Rc::new(binary_operators::new(&|f, s| f.max(s), 10)),
        );
        map.insert("dup".to_string(), Rc::new(DuplicatorOperator {}));

        Operations { map }
    }

    pub fn get(&self, command: &str) -> Option<Rc<dyn Operation>> {
        let should_append = command.starts_with("."); //indicates if command should just append rather than taking items from the stack
        let command = if should_append {
            command.split_at(1).1
        } else {
            command
        };

        self.map
            .get(command)
            .map(|op| Rc::clone(op))
            .or_else(|| {
                let i = command.parse::<f32>();
                i.map(|i| Rc::new(PushNumber { i }) as Rc<dyn Operation>)
                    .ok()
            })
            .or_else(|| {
                if command.starts_with("all.") && !command.starts_with("all.all.") {
                    let mut real_command = String::from(command);
                    let mut real_command = real_command.split_at(4).1;
                    println!("{}", real_command);

                    if let Some(operation) = self.get(real_command) {
                        if let Some(operation) = AllOperator::new(operation) {
                            Some(Rc::new(operation))
                        } else {
                            None
                        }
                    } else {
                        None
                    }
                } else {
                    None
                }
            })
            .map(|op| {
                if should_append {
                    Rc::new(NotTakingRealStack { real_operation: op })
                } else {
                    op
                }
            })
    }

    pub fn put(&mut self, command: String, oper: Rc<dyn Operation>) {
        self.map.insert(command, oper);
    }
}

struct NotTakingRealStack {
    real_operation: Rc<dyn Operation>,
}

//TODO: Add syntax ..<operation> for only printing the result and not modifying the stack at all
impl Operation for NotTakingRealStack {
    fn apply(&self, stack: &Vector<f32>) -> Result<(f32, Box<dyn Undoable>), OperationError> {
        self.real_operation.apply(stack).map(|(res, undo)| {
            (
                res,
                Box::new(NotTakingRealStackUndoable { res }) as Box<dyn Undoable>,
            )
        })
    }

    fn get_priority(&self) -> usize {
        self.real_operation.get_priority()
    }

    fn shortens_stack(&self) -> bool {
        false
    }
}

#[derive(Serialize, Deserialize)]
struct NotTakingRealStackUndoable {
    res: f32,
}

#[typetag::serde]
impl Undoable for NotTakingRealStackUndoable {
    fn undo(&self, stack: &mut Vector<f32>) {
        stack.pop_back();
    }

    fn redo(&self, stack: &mut Vector<f32>) {
        stack.push_back(self.res);
    }
}
