use crate::tree_util::NodeMarking::{Active, Finished};
use indextree::{Arena, NodeId};
use std::collections::HashMap;

pub struct DFSCallback<FTreeEdge, FNonTreeEdge, FBacktrack>
where
    FTreeEdge: FnMut(NodeId, NodeId),
    FNonTreeEdge: FnMut(NodeId, NodeId),
    FBacktrack: FnMut(NodeId, NodeId),
{
    pub traverse_tree_edge: FTreeEdge,
    pub traverse_non_tree_edge: FNonTreeEdge,
    pub backtrack: FBacktrack,
}

enum NodeMarking {
    Active,
    Finished,
}

/// Algorithms as seen in "Algorithmen und Datenstrukturen" DOI: 10.1007/978-3-642-05472-3
pub fn dfs<FTreeEdge, FNonTreeEdge, FBacktrack, T>(
    u: NodeId,
    v: NodeId,
    arena: &Arena<T>,
    callback: &mut DFSCallback<FTreeEdge, FNonTreeEdge, FBacktrack>,
) where
    FTreeEdge: FnMut(NodeId, NodeId),
    FNonTreeEdge: FnMut(NodeId, NodeId),
    FBacktrack: FnMut(NodeId, NodeId),
{
    let mut marked = HashMap::new();
    marked.insert(v, Active);

    for w in v.children(arena) {
        if marked.contains_key(&w) {
            (callback.traverse_non_tree_edge)(v, w);
        } else {
            (callback.traverse_tree_edge)(v, w);
            dfs(v, w, arena, callback);
        }
    }

    (callback.backtrack)(u, v);
    marked.insert(v, Finished);
}

pub fn dfs_explore<FTreeEdge, FNonTreeEdge, FBacktrack, T>(
    v: NodeId,
    arena: &Arena<T>,
    callback: &mut DFSCallback<FTreeEdge, FNonTreeEdge, FBacktrack>,
) where
    FTreeEdge: FnMut(NodeId, NodeId),
    FNonTreeEdge: FnMut(NodeId, NodeId),
    FBacktrack: FnMut(NodeId, NodeId),
{
    dfs(v, v, arena, callback);
}

pub struct DFSNumber {
    dfs_num: HashMap<NodeId, usize>,
    fin_num: HashMap<NodeId, usize>,
}

impl DFSNumber {
    pub fn is_ancestor(&self, v: NodeId, w: NodeId) -> bool {
        self.dfs_num[&v] < self.dfs_num[&w] && self.fin_num[&w] < self.fin_num[&v]
    }

    pub fn find_lca<T>(&self, v: NodeId, w: NodeId, arena: &Arena<T>) -> NodeId {
        if self.is_ancestor(v, w) {
            return v;
        } else if self.is_ancestor(w, v) {
            return w;
        }

        let mut parents = v.ancestors(arena);
        for parent in parents {
            if self.is_ancestor(parent, w) {
                return parent;
            }
        }

        panic!("Invariant that every node is an child of the root node has been destroyed.");
    }
}

pub fn dfs_number<T>(root: NodeId, arena: &Arena<T>) -> DFSNumber {
    let mut dfs_pos = 1;
    let mut fin_pos = 1;

    let mut dfs_num = HashMap::new();
    let mut fin_num = HashMap::new();

    dfs_num.insert(root, dfs_pos);
    dfs_pos += 1;

    dfs_explore(
        root,
        arena,
        &mut DFSCallback {
            traverse_tree_edge: |v, w| {
                dfs_num.insert(w, dfs_pos);
                dfs_pos += 1;
            },
            traverse_non_tree_edge: |_, _| {},
            backtrack: |u, v| {
                fin_num.insert(v, fin_pos);
                fin_pos += 1;
            },
        },
    );

    DFSNumber { dfs_num, fin_num }
}
